//
//  ViewController.m
//  ScrollView
//
//  Created by Jason Vantrease on 1/11/17.
//  Copyright © 2017 Jason Vantrease. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    UIScrollView * scrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    
    [self.view addSubview:scrollView];
    
    [scrollView setContentSize:CGSizeMake(self.view.frame.size.width *3, self.view.frame.size.height * 3)];
    
    scrollView.pagingEnabled = YES;
    // red view coord 0,1
    UIView* redView = [[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width, 0, self.view.frame.size.width, self.view.frame.size.height)];
    
    redView.backgroundColor = [UIColor redColor];
    
    [scrollView addSubview:redView];
    //blue view coord 0,0
    UIView* blueView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    
    blueView.backgroundColor = [UIColor blueColor];
    
    [scrollView addSubview:blueView];
    // yellow coord 1,0
    UIView* yellowView = [[UIView alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height)];
    
    yellowView.backgroundColor = [UIColor yellowColor];
    
    [scrollView addSubview:yellowView];
    //green view coord 1,1
    UIView* greenView = [[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width, self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height)];
    
    greenView.backgroundColor = [UIColor greenColor];
    
    [scrollView addSubview:greenView];
    // black coord 1,2
    UIView* blackView = [[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width * 2, self.view.frame.size.height, self.view.frame.size.width, self.view.frame.size.height)];
    
    blackView.backgroundColor = [UIColor blackColor];
    
    [scrollView addSubview:blackView];
    // gray coord 2,0
    UIView* grayView = [[UIView alloc]initWithFrame:CGRectMake(0, self.view.frame.size.height * 2, self.view.frame.size.width, self.view.frame.size.height)];
    
    grayView.backgroundColor = [UIColor grayColor];
    
    [scrollView addSubview:grayView];
    //purple coord 2,1
    UIView* purpleView = [[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width, self.view.frame.size.height *2, self.view.frame.size.width, self.view.frame.size.height)];
    
    purpleView.backgroundColor = [UIColor purpleColor];
    
    [scrollView addSubview:purpleView];
    // cyan coord 2,2
    UIView* cyanView = [[UIView alloc]initWithFrame:CGRectMake(self.view.frame.size.width * 2, self.view.frame.size.height * 2, self.view.frame.size.width, self.view.frame.size.height)];
    
    cyanView.backgroundColor = [UIColor redColor];
    
    [scrollView addSubview:cyanView];
    
    
    
    
                                 
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
